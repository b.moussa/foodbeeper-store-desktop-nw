export const AppConfig = {
  production: true,
  environment: "PROD",
  apiUrl: "https://api.foodbeeper.com/api/",
  appVersion: 20011,
  firebase: "fb_holder",
  version: require("../../package.json").version
};
