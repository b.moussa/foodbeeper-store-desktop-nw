module.exports = {
  production: false,
  environment: 'LOCAL',
  apiUrl: "https://staging.foodbeeper.com/api/",
  appVersion: 20011,
  firebase: "U2FsdGVkX1+UHcpfk/ONvA3oCZxYijBHdFJrlspFStglg/tEl7Q7ivwf7WuzzmvsgUPdTDLu7XrAzUqOI8Z0KkJKhTh41ftfRZw4MDSCQL//NT1ZF5dTr8pzbJRr5hxGX/nv5hjEMF3+CaimOpLt0MqPZDZgDKelNqqJwfIXGuLkSTazuOZ9tWdqTHkjVRBn6QX20MH0Yudoz8IJ+VvSrTPTq5jTqGjK8om+gT6WN6NsBZtmo2YTfpubBqPZT1UKh/JIVWNb1A2zxrBgkzmrQKC9TXNrZ0G4AIZOM/jDXHlAlS/1lLn4erAUVTMlwIC62E7jdQwKBusUQ+QQ65lVH2aeIEJOx8bPuO/llxGeseuLY6mbBv2Xwfb9sobr3wCLEu/OJK3egnzFtrFqC43P52gxvr5zHbDik3t+R58J8bvKztwrrueWoAkUlxYg3xdBAnESZjTfU3n3GHR2m32ct7qY95keqzDCRio5D1kQLbr1KeZ7NP1vcSGmiO/Y80rhW/p8jYK1WRbkMTV2yv55Eg==" ,
  version: require('../../package.json').version
}

