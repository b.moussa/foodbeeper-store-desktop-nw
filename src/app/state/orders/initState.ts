export interface  OrderModel {
  client?: any;
  created?: any;
  delivery?: any;
  items?: any[];
  profile?: any;
  store_id?: number;
  store_price?: number;
  type?: string;
  order_code?: string;
  order_number?: string;
  status?: string;
  id?: number;
}

export interface  OrdersState {
    new_orders?: OrderModel [];
    active_orders?: OrderModel [];
    new_order?: OrderModel;
    target_order?: OrderModel;
}

export const ordersInitState: any = {
  new_orders: [],
  active_orders: [],
  new_order: null,
  target_order: null
};
