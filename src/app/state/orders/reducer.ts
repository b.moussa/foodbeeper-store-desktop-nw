import {Action, createReducer, on, State} from '@ngrx/store';
import {OrdersAction} from './actions';
import { OrdersState, OrderModel, ordersInitState} from './initState';


const reducer = createReducer(
  ordersInitState,
  on(OrdersAction.addNewOrderToQueue, (state, { data }) => {
    console.log("we are inside reducer of oreder ",  data);
    return {...state  , new_orders: [...state.new_orders, data] };
  } ),
  on(OrdersAction.filterNewOrders, (state, { data }) => {
    return {...state  ,  new_orders: state.new_orders.filter(item => item.id !== data.id) };
  } ),
  on(OrdersAction.setNewOrder, (state, { data }) => {
    return {...state  ,  new_order: data };
  } ),
  on(OrdersAction.addActiveOrder, (state, { data }) => {
    return {...state  ,  active_orders: [...state.active_orders, data] };
  } ),
  on(OrdersAction.setTargetOrder, (state, { data }) => {
    return {...state  ,  target_order: data };
  } ),
  on(OrdersAction.filterActiveOrders, (state, { data }) => {
    return {...state  ,   active_orders: state.active_orders.filter(item => item.id !== data.id) };
  } ),
  on(OrdersAction.updateActiveOrders, (state, { data }) => {
    return {...state  ,  active_orders: state.active_orders.map( item => {
          console.log("updaterrrrrrrrrrrr ", item, " with ", data);
       if (item.id === data.id ){
         return data;
       }
       return item;
    }) };
  } ),
  on(OrdersAction.updateNewOrders, (state, { data }) => {
    return {...state  ,  new_orders: state.new_orders.map( item => {
      console.log("new_orders ", item, " with ", data.id);
      if (item.id === data.id ) {
        return data;
      }
      return item;
    }) };
  } ),
);

export function ordersReducer(state: State<any>| undefined, action: Action) {
  return reducer(state, action);
}
