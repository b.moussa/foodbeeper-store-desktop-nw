import { createAction, createSelector, props } from '@ngrx/store';
import {AppState} from "../appState";
import {OrderModel, OrdersState} from "./initState";
export const NEW_ORDER = '[Orders] NEW_ORDER';
export const NEW_ORDER_TO_QUEUE = '[Orders] NEW_ORDER_TO_QUEUE'
export const ADD_ACTIVE_ORDER = '[Orders] ADD_ACTIVE_ORDER'
export const FILTER_NEW_ORDERS = '[Orders] FILTER_NEW_ORDERS'
export const SET_TARGET_ORDER = '[Orders] SET_TARGET_ORDER'
export const FILTER_ACTIVE_ORDERS = '[Orders] FILTER_ACTIVE_ORDERS'
export const UPDATE_ACTIVE_ORDERS = '[Orders] UPDATE_ACTIVE_ORDERS'
export const UPDATE_NEW_ORDERS = '[Orders] UPDATE_NEW_ORDERS'


export const selectOrders = (state: AppState) => {
  return state.orders;
}

export const addNewOrderToQueue = createAction(
  NEW_ORDER_TO_QUEUE,
  props<{ data: OrderModel }>()
);
export const filterActiveOrders = createAction(
  FILTER_ACTIVE_ORDERS,
  props<{ data: OrderModel }>()
);

export const setNewOrder = createAction(
  NEW_ORDER,
  props<{ data: OrderModel }>()
);

export const setTargetOrder = createAction(
  SET_TARGET_ORDER,
  props<{ data: OrderModel }>()
);

export const updateActiveOrders = createAction(
  UPDATE_ACTIVE_ORDERS,
  props<{ data: OrderModel }>()
);
export const updateNewOrders = createAction(
  UPDATE_NEW_ORDERS,
  props<{ data: OrderModel }>()
);


export const filterNewOrders = createAction(
  FILTER_NEW_ORDERS,
  props<{ data: OrderModel }>()
);

export const addActiveOrder = createAction(
  ADD_ACTIVE_ORDER,
  props<{ data: OrderModel }>()
);



export const selectNewOrders = (state: AppState) => {
  return state.orders;
}

export const getNewOrders = createSelector(selectOrders, (state: OrdersState) => {
  return state.new_orders;
} );



//// new order to show
export const selectNewOrder = (state: AppState) => {
  return state.orders;
}
export const getNewOrder = createSelector(selectOrders, (state: OrdersState) => {
  return state.new_order;
} )
export const getTargetOrder = createSelector(selectOrders, (state: OrdersState) => {
  return state.target_order;
} )


export const getActiveOrders = createSelector(selectOrders, (state: OrdersState) => {
  return state.active_orders;
} )



export  const OrdersAction = {addNewOrderToQueue, updateActiveOrders, updateNewOrders, filterActiveOrders, getTargetOrder, setTargetOrder,
                              getNewOrders, setNewOrder, getNewOrder, getActiveOrders, filterNewOrders, addActiveOrder};




