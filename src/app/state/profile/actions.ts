import { createAction, createSelector, props } from '@ngrx/store';
import {AppState} from "../appState";
import {ProfileState} from "./initState";
export const AUTHENTICATE = '[Auth] authenticate'
export const UPDATE_PROFILE = '[PROFILE] UPDATE_PROFILE'

export const authenticateUser = createAction(
  AUTHENTICATE,
  props<{ data: any }>()
);

export const updateProfile = createAction(
  UPDATE_PROFILE,
  props<{ data: any }>()
);



export const selectProfile = (state: AppState) => {
  return state.profile;
}

export const getProfile = createSelector(selectProfile, (state: ProfileState) => {
  return state;
} );

export const getOnlineStatus = createSelector(selectProfile, (state: ProfileState) => {
  return state.user.connected;
} );


export  const ProfileAction = {authenticateUser, updateProfile, getProfile, getOnlineStatus};
