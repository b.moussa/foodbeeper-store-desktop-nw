import {Action, createReducer, on, State} from '@ngrx/store';
import {ProfileAction} from './actions';
import {initialState, ProfileState} from './initState';


const reducer = createReducer(
  initialState,
  on(ProfileAction.authenticateUser, (state, { data }) => {
    return {...state ,  ...data};
  } ),
  on(ProfileAction.updateProfile, (state, { data }) => {
    return {...state , user: {...state.user, ...data }  };
  } )
);

export function profileReducer(state: State<ProfileState> | undefined, action: Action) {
  return reducer(state, action);
}
