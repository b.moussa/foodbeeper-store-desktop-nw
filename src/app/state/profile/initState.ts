export interface  ProfileState {
  authenticated: boolean;
  token: string;
  firebaseToken: string;
  user: any;
  profile: any;
  fcmToken: string;
}


export const initialState: ProfileState = {
  authenticated: false,
  token : "",
  firebaseToken: "",
  user : {},
  fcmToken: "",
  profile: {}
};
