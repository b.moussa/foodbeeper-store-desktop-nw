import {ProfileState, initialState as profileInitState } from './profile/initState';
import {ordersInitState, OrdersState} from "./orders/initState";

export interface  AppState  {
  profile: ProfileState;
  orders: OrdersState;
}

export const initialState  = {
  profile: profileInitState,
  orders: ordersInitState
}

