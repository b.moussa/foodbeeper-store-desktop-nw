import { StoreModule as m } from '@ngrx/store';
import {profileReducer} from './profile/reducer';
import {metaReducers} from './logger';
import {initialState} from './appState';
import {ordersReducer} from "./orders/reducer";

export const StoreModule = m.forRoot({ profile: profileReducer, orders: ordersReducer}, {initialState: initialState,  metaReducers, runtimeChecks: {
  strictStateImmutability: true,
  strictActionImmutability: true,
  strictStateSerializability: true,
  strictActionSerializability: true,
}});
