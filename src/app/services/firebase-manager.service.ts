import { Injectable } from '@angular/core';
import {FirebaseService} from "./firebase.service";
import {OrderModel} from "../state/orders/initState";
import {Observable} from "rxjs/index";
import {Store, State} from "@ngrx/store";
import {AppState} from "../state/appState";
import { AudioService } from './audio.service';
import { OrderService } from './order.service';
import {DataSnapshot} from "@angular/fire/database/interfaces";
import {getOnlineStatus} from "../state/profile/actions";

@Injectable({
  providedIn: 'root'
})
export class FirebaseManagerService {
  activeOrders$: Observable<any[]>;

  constructor(private firebaseService: FirebaseService, private orderService: OrderService,
    private audioService: AudioService, private store: Store<AppState>, private state: State<AppState>) { }

  startService() {
        this.activeOrders$ = this.firebaseService.getActiveOrders();
        this.activeOrders$.subscribe((action: any ) => {
          const action_type = action.type;
          const order = action.payload.val();
          console.log("action type ====> ", action);
          switch (action_type) {
            case  'child_added':
                    setTimeout(() => {
                      this.handleAddedOrder(order);
                    }, 1000)
              break;
            case  'child_changed':
                    this.handleChangedOrder(order);
              break;

            case 'child_removed':
                    this.handleRemovedOrder(order);
               break;
          }
        });

        this.firebaseService.onConnect();
        setTimeout(()=>{
          this.firebaseService.profileChanges();
        }, 3000)
  }

  checkOnlineStatus () {
    return this.store.select(getOnlineStatus);
  }
  handleAddedOrder(order: OrderModel) {
    console.log("************ handleAddedOrder **************** ", order);
    if ( order.status === 'confirmed' ) {
      this.orderService.orderRecived(order.id);
      if ( this.state.getValue().orders.new_order === null  ) {
        this.orderService.newOrder(order);
      } else {
        console.log("add to queue ", order);
        this.orderService.addNewOrderToQueue(order);
      }
    } else {
      this.orderService.addAciveOrder(order);
    }
  }

  handleChangedOrder (order) {
    console.log("************ handleChangedOrder **************** ", order);
    if (order.status === 'confirmed') {
            if (this.state.getValue().orders.new_order !== null && this.state.getValue().orders.new_order.id === order.id ) {
                this.orderService.newOrder(order);
            } else {
              let  newOrderExist = false;
              this.state.getValue().orders.new_orders.forEach( item => {
                if ( item.id === order.id ) {
                  newOrderExist = true;
                  return;
                }
              })
              if( newOrderExist ) {
                this.orderService.updatNewOrders(order);
              } /*else {
                this.orderService.addNewOrderToQueue(order);
              }
             */
              this.orderService.filterActiveOrders(order);
            }
    } else {
        let  activeOrderExist = false;
        this.state.getValue().orders.active_orders.forEach( item => {
            if ( item.id === order.id ) {
              activeOrderExist = true;
              return;
            }
        })
      if ( activeOrderExist) {
        this.orderService.updateActiveOrders(order);
      } else {
          this.orderService.addAciveOrder(order);
      }
      if (this.state.getValue().orders.new_order !== null && this.state.getValue().orders.new_order.id === order.id ) {
        this.orderService.newOrder(null);
      }
      if (this.state.getValue().orders.target_order !== null && this.state.getValue().orders.target_order.id === order.id ) {
        this.orderService.setTargetOrder(order);
      }
      this.orderService.filterNewOrders(order);
    }
  }

  handleRemovedOrder (order) {
      console.log("************ handleRemovedOrder **************** ", order);
      this.orderService.filterActiveOrders(order);
      this.orderService.filterNewOrders(order);
      if ( this.state.getValue().orders.new_order &&  this.state.getValue().orders.new_order.id === order.id ) {
        this.orderService.newOrder(null);
      }
    if ( this.state.getValue().orders.target_order && this.state.getValue().orders.target_order.id === order.id ) {
      this.orderService.setTargetOrder(null);
    }
  }


}
