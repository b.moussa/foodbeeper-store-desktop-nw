import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
const AppConfig = require("../../environments/environment");
@Injectable({
  providedIn: 'root'
})
export class AuthApiService {

  constructor(
    private httpserviceService: HttpService,
    ) { }

  connect(data) {
    return this.httpserviceService.post(data, "authenticate");
  }

  authenticatedUser() {
    return this.httpserviceService.get("authenticated_user?app_version=" + AppConfig.appVersion);
  }

  refreshtoken() {
    return this.httpserviceService.get("refresh_token");
  }

  logout() {
    const token = localStorage.getItem("token");
    const settings = {
      "token": token,
    };
    return this.httpserviceService.post(settings, "logout");
  }

  checkLogin() {
    return this.authenticatedUser();
  }

}
