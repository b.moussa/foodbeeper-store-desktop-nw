import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { Observable, throwError } from "rxjs";

import { catchError, retry } from "rxjs/operators";
const AppConfig = require("../../environments/environment");
import { ToastService } from "../toasts/toast.service";
@Injectable({
  providedIn: "root"
})
export class HttpService {
  baseUrl = AppConfig.apiUrl;
  headers;
  httpOptions;
  constructor(
    private router: Router,
    private toastService: ToastService,
    private http: HttpClient
  ) {}

  initHeaders() {
    console.log("init headers ");
    const token = localStorage.getItem("token");
    console.log("our token  ", token);
    this.httpOptions = {
      headers: new HttpHeaders({
        Authorization: "Bearer " + token
      }),
      observe: "response"
    };
    console.log("http option ", this.httpOptions.headers.get("Authorization"));
  }

  post(data, endpoint): Observable<any> {
    this.initHeaders();
    console.log("send ", endpoint, " data ==> ", data);
    console.log(",,,,,, ,, ", this.httpOptions);
    return this.http
      .post<any>(this.baseUrl + endpoint, data, this.httpOptions)
      .pipe(catchError(err => this.handleError(endpoint, err)));
  }

  get(operation): Observable<any> {
    this.initHeaders();
    return this.http
      .get<any>(this.baseUrl + operation, { headers: this.httpOptions.headers })
      .pipe(
        retry(3),
        catchError(err => this.handleError(operation, err))
      );
  }

  delete(id, operation) {
    this.initHeaders();
    return this.http
      .delete(this.baseUrl + operation + "/" + id, { headers: this.headers })
      .pipe(catchError(err => this.handleError(operation + " " + id, err)));
  }

  handleError(endpoint, error: any) {
    console.log("error statsu ", error.status);
    console.log("we have error ", endpoint);
    console.log("fffffffffff ", error);
    switch (error.status) {
      case 401: {
        this.toastService.show(error.error.message, {
          classname: "bg-danger text-light",
          delay: 10000
        });

        return throwError({ status: 401, errors: error.error.message });
      }

      case 404: {
        this.toastService.show(error.error.message, {
          classname: "bg-danger text-light",
          delay: 10000
        });

        return throwError({ status: 404, errors: error.error.message });
      }

      case 403: {
        this.toastService.show(error.error.message, {
          classname: "bg-danger text-light",
          delay: 10000
        });

        return throwError(error.error || "Server error");
      }

      case 422: {
        this.toastService.show(error.error.message, {
          classname: "bg-danger text-light",
          delay: 10000
        });

        return throwError({ status: 422, errors: error.error.message });
      }

      case 500: {
        this.toastService.show("Server Error", {
          classname: "bg-danger text-light",
          delay: 10000
        });

        this.router.navigateByUrl("fatal");
        return throwError({ status: 500, errors: error.error.message });
      }
      default:
        return throwError({ status: 0, errors: error.error.message });
    }
  }
}
