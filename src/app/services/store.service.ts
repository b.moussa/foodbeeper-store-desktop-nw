import { Injectable } from '@angular/core';
import {HttpService} from './http.service'
import {AppState} from "../state/appState";
import {Store, State} from "@ngrx/store";
import {updateProfile} from '../state/profile/actions'
@Injectable({
  providedIn: 'root'
})
export class StoreService {

  constructor(private httpService: HttpService, private store: Store<AppState>, private state: State<AppState>) { }


  getStoreInformation() {
   return this.httpService.get('store/' + this.state.getValue().profile.profile.store.id);
  }

  getStoreTransactions() {
    return this.httpService.get("store_transactions?order_by=-created");
  }

  updateStoreDeliveryStatus(data) {
    return this.httpService.post(data, 'store/' + this.state.getValue().profile.profile.store.id);
  }

  updateStoreProfile(data) {
    return this.store.dispatch(updateProfile({data}));
  }

  getStoreRating() {
    return this.httpService.get("store_ratings/" + this.state.getValue().profile.profile.store.id + "?order_by=-created");
  }
}
