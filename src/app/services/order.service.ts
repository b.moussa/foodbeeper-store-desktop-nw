import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { AppState } from '../state/appState';
import {State, Store} from '@ngrx/store';
import {
  addNewOrderToQueue, getTargetOrder, addActiveOrder, getNewOrder, setTargetOrder, getActiveOrders, setNewOrder,
  filterActiveOrders, filterNewOrders, updateActiveOrders, updateNewOrders
} from '../state/orders/actions';
import {FirebaseService} from "./firebase.service";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private httpService: HttpService, private firebaseService: FirebaseService, private store: Store<AppState>, private state: State<AppState> ) { }

  updateOrder(id, data) {
    return this.httpService.post(data, 'order/' + id).toPromise();
  }
  rejectOrder(order, cause) {
    console.log("state is ", this.state.getValue());
    const data = {
      type: 'order_status',
      status: "rejected",
      store_id: this.state.getValue().profile.profile.store.id,
      user_id: this.state.getValue().profile.id,
      order_id: order.id,
      comment: cause
    };
   return this.updateOrderStatus(data);
  }
  newOrder (order) {
    this.store.dispatch(setNewOrder({data: order}));
  }

  addNewOrderToQueue (order) {
    this.store.dispatch(addNewOrderToQueue({data: order}));
  }

  addAciveOrder(order) {
    this.store.dispatch(addActiveOrder({data: order}));
  }

  getNewOrder() {
    return this.store.select(getNewOrder);
  }

  setTargetOrder(order) {
    this.store.dispatch(setTargetOrder({data: order}));
  }
  getTargetOrder() {
    return this.store.select(getTargetOrder);
  }
  getActiveOrders() {
    return this.store.select(getActiveOrders);
  }

  checkNewOrderQueue() {
    console.log("********************** check new Queue ", this.state.getValue().orders.new_orders.length);
    if(this.state.getValue().orders.new_orders.length >0 ){
      const order = this.state.getValue().orders.new_orders[0]
      console.log("zzzzzzzzzzzzzzzzzzzzzzzzzzz  ", order);
      this.newOrder(order);
      this.filterNewOrders(order);
    }
  }

  filterActiveOrders(order) {
    this.store.dispatch(filterActiveOrders({data: order}));
  }
  filterNewOrders(order) {
   this.store.dispatch(filterNewOrders({data: order}));
  }

  updateActiveOrders(order) {
    this.store.dispatch(updateActiveOrders({data: order}));
  }
  updatNewOrders(order) {
    this.store.dispatch(updateNewOrders({data: order}));
  }

  updateOrderStatus(data) {
    console.log("pushing data ????? ", data);
    return this.firebaseService.push('commands', data);
  }

  orderRecived(id) {
    console.log("send order reccived ", id);
    return this.httpService.post({}, "order_received/"+ id ).toPromise();
    }
}
