import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";
import { Store, State } from "@ngrx/store";
import { AppState } from "../state/appState";
@Injectable({
  providedIn: "root"
})
export class MenuService {
  constructor(
    private httpserviceService: HttpService,
    private state: State<AppState>
  ) {}

  getMenu() {
    return this.httpserviceService.get(
      "menu_tree/" + this.state.getValue().profile.profile.store.id
    );
  }
  getOptions() {
    return this.httpserviceService.get(
      "menu/" +
        this.state.getValue().profile.profile.store.id +
        "/item_options_groups_tree"
    );
  }

  deleteItem(storeId, itemId) {
    return this.httpserviceService.delete(itemId, "menu/" + storeId + "/item");
  }

  updateItem(data, id) {
    return this.httpserviceService.post(
      data,
      "menu/" + this.state.getValue().profile.profile.store.id + "/item/" + id
    );
  }

  addItem(storeId, data) {
    return this.httpserviceService.post(data, "menu/" + storeId + "/item");
  }

  updateOption(data, id) {
    return this.httpserviceService.post(
      data,
      "menu/" +
        this.state.getValue().profile.profile.store.id +
        "/item_option/" +
        id
    );
  }
  addOptionGroup(storeId, data) {
    return this.httpserviceService.post(
      data,
      "menu/" + storeId + "/item_options_group"
    );
  }
  addOption(storeId, data) {
    return this.httpserviceService.post(
      data,
      "menu/" + storeId + "/item_option"
    );
  }
  updateOptionGroup(storeId, groupId, data) {
    return this.httpserviceService.post(
      data,
      "menu/" + storeId + "/item_options_group/" + groupId
    );
  }
  addSection(storeId, data) {
    return this.httpserviceService.post(data, "menu/" + storeId + "/section");
  }
  updateSection(storeId, sectionId, data) {
    return this.httpserviceService.post(
      data,
      "menu/" + storeId + "/section/" + sectionId
    );
  }
  deleteSection(storeId, sectionId) {
    return this.httpserviceService.delete(
      sectionId,
      "menu/" + storeId + "/section"
    );
  }
}
