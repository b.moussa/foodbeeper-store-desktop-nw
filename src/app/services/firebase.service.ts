import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList, AngularFireObject} from "@angular/fire/database";
import {ProfileAction, updateProfile} from "../state/profile/actions";
import {Store} from "@ngrx/store";
import {AppState} from "../state/appState";
import {FirebaseListObservable} from "@angular/fire/database-deprecated";
import {Observable} from "rxjs/index";
import {DatabaseReference} from "@angular/fire/database/interfaces";
import {StoreService} from "./store.service";

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {
  private activeOrders$: AngularFireList<any>;
  private connectRef$: DatabaseReference;
  private storeOnlineRef$:  DatabaseReference;
  private onlineStoresRef$:  DatabaseReference;
  conInterval: any = 0 ;
  profile;

  public isConnected = false;
  constructor(private db: AngularFireDatabase,
              private storeService: StoreService,
              private store: Store<AppState>) {
    this.store.select(ProfileAction.getProfile).subscribe(res => {
      this.profile = res.profile;
      console.log("ressssssssssssssssssssssssssss ", res);
      if(!this.profile.store) {return; }
      this.activeOrders$ = this.db.list('active_orders/' + this.profile.store.region_id + '/' + this.profile.store.id);
     this.connectRef$ =  this.db.database.ref(".info/connected");
    });
  }

  getActiveOrders (): Observable <any> {
    return this.activeOrders$.stateChanges(['child_added', 'child_changed', 'child_removed']);
  }

  onConnect() {
    this.connectRef$.on("value", snap => {
      console.log("snapshot value " + snap.val());
      if (!this.isConnected) {
          this.presence();
          clearInterval(this.conInterval);
      } else {
        console.log("disconnected ");
        this.onDisconnect();
      }

      this.isConnected = snap.val();
      this.store.dispatch(updateProfile({data: {connected: this.isConnected}}));
    });

  }

  profileChanges() {
    this.onlineStoresRef$.on("value", snapshot => {
      console.log('_changedchild_changed '+ JSON.stringify(snapshot.val()) );
      this.storeService.updateStoreProfile(snapshot.val());
    });
  }

  onDisconnect() {
    this.conInterval = setInterval( () => {
      console.log("try to connect to firebase......");
      this.presence();
    }, 5000);
  }

  presence() {


    this.storeOnlineRef$ = this.db.database.ref("stores/online/" +  this.profile.store.id);
    this.storeOnlineRef$.onDisconnect().remove();
    this.storeOnlineRef$.update({name: 'joker'
    });

    this.onlineStoresRef$ = this.db.database.ref("online/" + this.profile.store.region_id + "/stores/" + this.profile.store.id);

    this.onlineStoresRef$.onDisconnect().remove();
    this.onlineStoresRef$.update({name:  this.profile.store.name, id: this.profile.store.id });

    this.onlineStoresRef$.on("child_added",(snap ) => {

      if ( snap.key === 'support_phone') {
        this.store.dispatch(updateProfile({data: {'support_phone' : snap.val() }}));
      } else if ( snap.key === 'new_ratings') {
        this.store.dispatch(updateProfile({data: {'new_ratings' : snap.val() }}));
      } else if ( snap.key === 'clients') {
        this.store.dispatch(updateProfile({data: {'clients' : snap.val() }}));
      }

    });

    this.connectRef$.on("value", function(snap) {
            console.log("   store.dispatch(isConnectedToFirebase( snap.val())) ", snap.val());
    });

  }
  update(key: string, value: any ): Promise<any> {
    return this.activeOrders$.update(key, value);
}

push(key: string, value: any ): Promise<any> {
  const ref = this.db.list(key);
  console.log("pushing ", value);
  return ref.push(value);
}



}
