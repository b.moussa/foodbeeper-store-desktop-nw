import { Pipe, PipeTransform } from "@angular/core";
import { TranslateServices } from "./translate.service";
@Pipe({
  name: "translate",
  pure: false
})
export class TranslatePipe implements PipeTransform {
  constructor(private translate: TranslateServices) {}
  transform(key: any): any {
    return this.translate.data[key] || key;
  }
}
