import { Injectable } from '@angular/core';
import {HttpService} from "./http.service";

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  static settings: any;
  constructor() {}
  fireConfig() {
    AppConfigService.settings = window['fireConfig'];
    return window['fireConfig']
  }
}
