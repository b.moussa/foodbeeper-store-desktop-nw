import { Injectable } from '@angular/core';
import {HttpService} from "./http.service";

@Injectable({
  providedIn: 'root'
})
export class OrderHistoryService {

  constructor(
    private httpserviceService: HttpService,
  ) { }

  getOrdersHistory(page = 1) {
    return this.httpserviceService.get("orders?order_by=-updated&page=" + page);
  }
}
