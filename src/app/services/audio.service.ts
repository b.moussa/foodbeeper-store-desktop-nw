import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AudioService {
  audio : HTMLAudioElement;
  constructor() {
      this.audio = new Audio();
      this.audio.src = "assets/audio/google.mp3"; 
      this.audio.load();
      this.audio.loop = true;
   }

  playNotifiation() {
    this.audio.play();
  }
  stopNotification() {
    this.audio.pause();
    this.audio.currentTime = 0;
  }
}
