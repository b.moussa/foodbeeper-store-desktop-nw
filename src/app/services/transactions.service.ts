import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {
  constructor(
    private httpserviceService: HttpService,
  ) { }

  getTransactions(page = 1) {
    return this.httpserviceService.get("store_transactions?order_by=-created&page=" + page);
  }
}
