import "reflect-metadata";
import "../polyfills";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule, APP_INITIALIZER } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";
import {AngularFireModule, FirebaseOptionsToken} from "@angular/fire";
import { HttpClientModule, HttpClient } from "@angular/common/http";
const AppConfig = require("../environments/environment");
import { NgxPrintModule } from "ngx-print";
import { NgxPrinterModule } from "ngx-printer";
import { TranslateServices } from "../app/services/translate.service";
import { AppRoutingModule } from "./app-routing.module";
import { SortableModule } from "ngx-bootstrap/sortable";
declare  var fireConfig:any;
// NG Translate
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";


import { WebviewDirective } from "./directives/webview.directive";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { SplashScreenComponent } from "./components/splash-screen/splash-screen.component";
import { StoreModule } from "./state/state";
import { OrdersComponent } from "./components/orders/orders.component";
import { BottomComponent } from "./bottom/bottom.component";
import { HolderComponent } from "./components/orders/holder/holder.component";
import { MenuComponent } from "./components/menu/menu.component";
import { TransactionsComponent } from "./components/transactions/transactions.component";
import { MenuItemsComponent } from "./components/menu/menu-items/menu-items.component";
import { OptionsComponent } from "./components/menu/options/options.component";
import { OrderHistoryComponent } from "./components/order-history/order-history.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MapToIterablePipe } from "./pipes/map-to-iterable.pipe";
import { LoadingSpinnerComponent } from "./components/loading-spinner/loading-spinner.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AngularFireDatabaseModule } from "@angular/fire/database";
import { AideComponent } from "./components/aide/aide.component";
import { AppBarComponent } from "./components/app-bar/app-bar.component";
import { ToastsComponent } from "./toasts/toasts.component";
import { SplashComponent } from "./components/splash/splash.component";
import { ParametersComponent } from "./components/parameters/parameters.component";
import { EditOptionsComponent } from "./components/parameters/edit-options/edit-options.component";
import { EditMenuComponent } from "./components/parameters/edit-menu/edit-menu.component";
import { OpeningHoursComponent } from "./components/parameters/opening-hours/opening-hours.component";
import { MenuSettingsComponent } from "./components/parameters/menu-settings/menu-settings.component";
import { TranslatePipe } from "./services/translate.pipe";
import {NwServices} from "./providers/nwServices";
import * as firebase from "firebase";
import {AppConfigService} from "./services/config.service";
import {ConfigModule} from "./modules/config/config.module";

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "../assets/i18n/", ".json");
}
export function setupTranslateFactory(service: TranslateServices): Function {
  return () => service.use(localStorage.getItem("lang"));
}

export function initializeApp(appConfig: AppConfigService) {
  return appConfig.fireConfig()
}


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WebviewDirective,
    LoginComponent,
    SplashScreenComponent,
    OrdersComponent,
    BottomComponent,
    HolderComponent,
    MenuComponent,
    TransactionsComponent,
    MenuItemsComponent,
    OptionsComponent,
    OrderHistoryComponent,
    MapToIterablePipe,
    LoadingSpinnerComponent,
    AideComponent,
    AppBarComponent,
    ToastsComponent,
    SplashComponent,
    ParametersComponent,
    EditOptionsComponent,
    EditMenuComponent,
    OpeningHoursComponent,
    MenuSettingsComponent,
    TranslatePipe
  ],

  imports: [
    NgxPrinterModule.forRoot({ printOpenWindow: false }),
    BrowserModule,
    BrowserAnimationsModule,
    AngularFireModule,
    AngularFireDatabaseModule,
    NgbModule,
    StoreModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxPrintModule,
    AppRoutingModule,
    SortableModule.forRoot(),
    FlexLayoutModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    {
      provide: FirebaseOptionsToken,
      deps: [AppConfigService],
      useFactory: initializeApp
    },
    NwServices,
    TranslateServices,
    {
      provide: APP_INITIALIZER,
      useFactory: setupTranslateFactory,
      deps: [TranslateServices],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(){
    console.log("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz ", fireConfig);
   //  AngularFireModule.initializeApp(fireConfig);
    firebase.initializeApp(fireConfig)
  }
}
