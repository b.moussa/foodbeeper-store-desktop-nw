import { HomeComponent } from "./components/home/home.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./components/login/login.component";
import { AuthGuardGuard } from "./guards/auth-guard.guard";
import { LoginGuardGuard } from "./guards/login-guard.guard";
import { OrdersComponent } from "./components/orders/orders.component";
import { MenuComponent } from "./components/menu/menu.component";
import { TransactionsComponent } from "./components/transactions/transactions.component";
import { OrderHistoryComponent } from "./components/order-history/order-history.component";
import { AideComponent } from "./components/aide/aide.component";
import { ParametersComponent } from "./components/parameters/parameters.component";
import { OptionsComponent } from "./components/menu/options/options.component";
import { EditOptionsComponent } from "./components/parameters/edit-options/edit-options.component";
import { EditMenuComponent } from "./components/parameters/edit-menu/edit-menu.component";
import { OpeningHoursComponent } from "./components/parameters/opening-hours/opening-hours.component";
import { MenuSettingsComponent } from "./components/parameters/menu-settings/menu-settings.component";
const routes: Routes = [
  {
    path: "login",
    component: LoginComponent,
    canActivate: [LoginGuardGuard]
  },
  {
    path: "",
    component: HomeComponent,
    canActivate: [AuthGuardGuard],
    children: [
      { path: "", redirectTo: "orders", pathMatch: "full" },
      { path: "orders", component: OrdersComponent },
      { path: "aide", component: AideComponent },
      { path: "parametres", component: ParametersComponent },
      { path: "menu", component: MenuComponent },
      { path: "transactions", component: TransactionsComponent },
      { path: "orders-history", component: OrderHistoryComponent },
      { path: "options", component: OptionsComponent },
      { path: "editOptions", component: EditOptionsComponent },
      { path: "editMenu", component: EditMenuComponent },
      { path: "OpeningHours", component: OpeningHoursComponent },
      { path: "menuSettings", component: MenuSettingsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
