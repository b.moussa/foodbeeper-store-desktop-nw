import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-bottom',
  templateUrl: './bottom.component.html',
  styleUrls: ['./bottom.component.scss']
})
export class BottomComponent implements OnInit {
  @Input('solde') storeSold = '-';
  constructor(private router: Router) { }

  ngOnInit() {
  }
  navigateToSold() {
    this.router.navigateByUrl('transactions');
  }
}
