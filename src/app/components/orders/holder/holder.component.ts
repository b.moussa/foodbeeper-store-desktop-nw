import {Component, NgZone, OnInit} from '@angular/core';
import {AppState} from "../../../state/appState";
import {Store, State} from "@ngrx/store";
import {getProfile} from "../../../state/profile/actions";
@Component({
  selector: 'app-holder',
  templateUrl: './holder.component.html',
  styleUrls: ['./holder.component.scss']
})
export class HolderComponent implements OnInit {
  profile;
  deliveryStatus = 0;
  profile$
  constructor(private state: State<AppState>,private  zone: NgZone, private store: Store<AppState> ) {
    this.profile$ = this.store.select(getProfile);

  }

  ngOnInit() {
    this.profile = this.state.getValue().profile;
    this.profile$.subscribe(res => {
      if ( res.user  ) {
        this.zone.run(() => {
          this.deliveryStatus = res.user.accept_delivery;
        });
      }
    });

  }

}
