import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Observable} from "rxjs/index";
import {OrderModel} from "../../state/orders/initState";
import {AppState} from "../../state/appState";
import {State, Store} from "@ngrx/store";
import { OrderService } from '../../services/order.service';
import { AudioService } from '../../services/audio.service';
import {getProfile} from "../../state/profile/actions";
@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  neweOrders$: Observable<OrderModel[]>;
  @Output("onReady") onReady = new EventEmitter();
  showLoading = -1;
  storeSold = "-";
  @Output("onDelivered") onDelivered = new EventEmitter();
  constructor(private orderService: OrderService, private store: Store<AppState>,
               private audioService: AudioService,
              private state: State<AppState>
  ) {
    this.store.select(getProfile).subscribe(res => {
      this.storeSold = res.user.money;
    })

  }

  ngOnInit() {
    this.neweOrders$ = this.orderService.getActiveOrders();
  }

  openOrder (order) {

    this.orderService.setTargetOrder(order);
  }

  orderReady(order, i) {
        this.showLoading = i;
        this.updateOrder(order.id, 'ready');
  }
  orderDelivered(order, i) {
    this.showLoading = i;
    this.updateOrder(order.id, 'in-delivery');
  }
 async updateOrder(id, status) {
    const data = {
      type: 'order_status',
      store_id: this.state.getValue().profile.profile.store.id,
      user_id: this.state.getValue().profile.id,
      order_id: id,
      status: status,
      comment: ''
    };
    try {
      await this.orderService.updateOrderStatus(data)
      this.orderService.setTargetOrder(null);
      this.hideLoader();
    } catch (err) {
      this.hideLoader()
      console.error("error ", err);
    }
  }
  hideLoader() {
    setTimeout(() => {
      this.showLoading = -1;
    }, 2500 )
  }

}
