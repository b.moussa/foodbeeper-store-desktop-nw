import { Component, OnInit } from '@angular/core';
import {OrderHistoryService} from "../../services/order-history.service";

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.component.html',
  styleUrls: ['./order-history.component.scss']
})
export class OrderHistoryComponent implements OnInit {
  orders = new Array();
  selectedIndex;
  activePage = 1;
  pages = [1, 2 , 3, 4];
  meta;
  constructor(private orderHistoryService: OrderHistoryService) { }
  expandOrder(order, index) {
    this.selectedIndex =  this.selectedIndex !== index ? index : -1;
  }
  ngOnInit() {
    this.getHistory(1);
  }

  scrollTabContentToTop() : void {
    const elmnt =  document.getElementById("scroll")
    setTimeout(()=>{elmnt.scrollTop = 0;}
    )
  }
  getHistory(page) {

    this.orderHistoryService.getOrdersHistory(page).subscribe(res => {
      console.log("response of history ", res);
      this.orders = [...res.data];
      this.meta = res.meta;
      this.scrollTabContentToTop();
    });
  }
  nextPage(page) {
    if(this.meta.pagination.total_pages < page ) {
      return;
    }
    if( page > this.pages[3]) {
      console.log("xxxxxxxxxxxxxx")
      this.pages = this.pages.slice(1, this.pages.length);
      this.pages.push(page);
    }
    if( page < this.pages[0]) {
      console.log("current ipages *********  ", this.pages);
      this.pages.unshift(page);
      console.log("current ipages *********  ", this.pages);
      this.pages = this.pages.slice(0, this.pages.length - 1);
      console.log("current ipages *********  ", this.pages);
    }

    this.activePage = page;
      this.getHistory(page);
  }
}
