import {
  Component,
  ElementRef,
  NgZone,
  OnInit,
  ViewChild
} from "@angular/core";
import { Router } from "@angular/router";
import { NgbModal, NgbModalConfig } from "@ng-bootstrap/ng-bootstrap";
import { Observable } from "rxjs/index";
import { AppState } from "../../state/appState";
import { Store, State } from "@ngrx/store";
import { OrderModel } from "../../state/orders/initState";
import { FirebaseManagerService } from "../../services/firebase-manager.service";
import { OrderService } from "../../services/order.service";
import { AudioService } from "../../services/audio.service";
import { ToastService } from "../../toasts/toast.service";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { NgxPrinterService } from "ngx-printer";
import { OrdersComponent } from "../orders/orders.component";
import { NwServices } from 'src/app/providers/nwServices';

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  showDiv = false;
  @ViewChild("svgg", { static: true }) mySvg: ElementRef;
  newOrder: OrderModel;
  submitBnText = "Confirmer";
  @ViewChild("content", { static: true }) modal: any;
  @ViewChild("rejectModal", { static: true }) rejectModal;
  @ViewChild("nonDispoItems", { static: false }) nonDispoItems;

  selectedTime = -1;
  targetType;
  showLoading = false;
  refuseCause = null;
  isConnected = false;
  orderChecked = false;
  printing = false;
  clicked = 0;
  collapsed = false;
  isNewOrder = false;
  storeSold = "-";
  rejectedItems = [];
  formItems: FormGroup;
  connectStatus$: Observable<boolean>;
  @ViewChild("scrol", { static: true }) tabsContentRef: ElementRef; // Using "definite assignment" assertion (query).
  @ViewChild(OrdersComponent, { static: false, read: ElementRef })
  PrintComponent: ElementRef;
  constructor(
    private printerService: NgxPrinterService,
    private zone: NgZone,
    private toastService: ToastService,
    private formBuilder: FormBuilder,
    private firebaseManager: FirebaseManagerService,
    config: NgbModalConfig,
    private store: Store<AppState>,
    private orderService: OrderService,
    private modalService: NgbModal,
    private audioService: AudioService,
    public router: Router,
    private nwServices: NwServices,
    public state: State<AppState>
  ) {
    config.backdrop = "static";
    config.keyboard = false;
    this.formItems = this.formBuilder.group({
      rejectedItems: new FormArray([])
    });
  }

  onCheckChange(event) {
    const formArray: FormArray = this.formItems.get(
      "rejectedItems"
    ) as FormArray;
    if (event.target.checked) {
      formArray.push(new FormControl(event.target.value));
    } else {
      let i = 0;
      formArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value == event.target.value) {
          formArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  handleOrderStatus() {
    switch (this.newOrder.status) {
      case "confirmed":
        if (localStorage.getItem("lang") == "en") this.submitBnText = "Confirm";
        else {
          if (localStorage.getItem("lang") == "ar") this.submitBnText = "تأكيد";
          else this.submitBnText = "Confirmer";
        }

        break;
      case "accepted":
        if (localStorage.getItem("lang") == "en") this.submitBnText = "Ready?";
        else {
          if (localStorage.getItem("lang") == "ar")
            this.submitBnText = "جاهزة؟";
          else this.submitBnText = "prete?";
        }
        break;
      case "ready":
        if (localStorage.getItem("lang") == "en")
          this.submitBnText = "Payed order";
        else {
          if (localStorage.getItem("lang") == "ar")
            this.submitBnText = "الطلب مدفوع";
          else this.submitBnText = "commabde payé";
        }
        break;
    }
  }

  scrollTabContentToTop(): void {
    const elmnt = document.getElementById("scroll");
    setTimeout(() => {
      console.log("scrolllllllllllllllllllllllllllllllllllll");
      elmnt.scrollTop = 0;
    });
  }
  setRejectCause(i) {
    if (i === 0) {
      this.refuseCause = "Non disponible";
    } else if (i === 1) {
      this.refuseCause = "Restaurant Surcharge";
    }
  }

  hideLoader() {
    setTimeout(() => {
      this.showLoading = false;
    }, 1500);
  }
  async rejectOrder() {
    if (this.refuseCause === "Non disponible") {
      this.modalService.open(this.nonDispoItems, { centered: true });
    } else {
      this.updateRjectOrder();
    }
  }

  rejectItems() {
    const data = "Non disponible: " + this.formItems.value.rejectedItems;
    this.updateRjectOrder(data);
  }

  async updateRjectOrder(cause?) {
    this.showLoading = true;
    try {
      await this.orderService.rejectOrder(
        this.newOrder,
        cause || this.refuseCause
      );
      this.targetType = "";
      this.targetType = "";
      setTimeout(() => {
        this.hideLoader();
      }, 1000);
      this.selectedTime = -1;
    } catch (err) {
      console.log("error updating ", err);
      this.hideLoader();
    }
  }
  isDisconnectedCollapsed() {
    if (this.collapsed) {
      this.nwServices.expandScreen(150, 434);
      this.collapsed = false;
    }
  }
  ngOnInit() {
    this.nwServices.minimizeScreen();
    this.collapsed = true;
    setTimeout(() => {
      this.firebaseManager.startService();
      this.orderService.getNewOrder().subscribe(res => {
        console.log("orderService.getNewOrder ", res);
        console.log("this.targetType this.targetType ", this.targetType);
        this.newOrder = res;

        if (res == null) {
          this.isNewOrder = false;
          this.modalService.dismissAll();
          this.audioService.stopNotification();
          if (this.targetType === "new_order") {
            this.targetType = "";
          }
          this.checkNewOrder();
        } else {
          if (
            this.targetType === "new_order" &&
            this.state.getValue().orders.new_order !== null &&
            res.id === this.state.getValue().orders.new_order.id
          ) {
            return;
          }
          this.nwServices.remote.getCurrentWindow().center();
          this.nwServices.remote.getCurrentWindow().restore();

          this.nwServices.remote.getCurrentWindow().show();

          this.nwServices.remote.getCurrentWindow().focus();

          this.isNewOrder = true;
          this.targetType = "new_order";
          this.showDiv = true;
          this.nwServices.expandScreen();
          this.handleOrderStatus();
          this.audioService.playNotifiation();
          this.nwServices.expandScreen();
          this.collapsed = false;

          this.openOrderModal();
          this.scrollTabContentToTop();
        }
      });
      this.connectStatus$ = this.firebaseManager.checkOnlineStatus();
      this.connectStatus$.subscribe(res => {
        console.log("zzzzzzzzzzzzzzzzzzzz ", res);
        this.zone.run(() => {
          this.isConnected = res;
        });
      });
    }, 1000);

    this.orderService.getTargetOrder().subscribe(res => {
      console.log(
        "orderService.getTargetOrder ",
        this.modalService.hasOpenModals()
      );
      console.log("target order ", res);
      console.log("this.getTargetOrder this.getTargetOrder ", this.targetType);

      this.newOrder = res;
      if (res === null) {
        this.modalService.dismissAll();
        if (this.targetType === "target_order") {
          this.targetType = "";
        }
      } else {
        if (
          this.targetType === "target_order" &&
          this.state.getValue().orders.target_order !== null &&
          res.id === this.state.getValue().orders.target_order.id
        ) {
          return;
        }
        this.targetType = "target_order";
        this.showDiv = true;
        this.nwServices.expandScreen();
        this.handleOrderStatus();
        this.openOrderModal();
        this.scrollTabContentToTop();
        this.collapsed = false;
      }
    });
  }

  handleModalClose(modal) {
    this.orderChecked = false;
    if (this.newOrder.status === "confirmed") {
      this.openOrderRejectModal();
    } else {
      modal.close("Close click");
      this.targetType = "";
      this.orderService.setTargetOrder(null);
    }
  }
  removeAlert(event) {
    console.log("remove class ", event);
    event.classList.remove("animateNewOrder");
  }
  clearModal() {
    console.log("close modal ", this.targetType);
    if (this.targetType === "new_order") {
      this.orderService.newOrder(null);
    } else if (this.targetType === "target_order") {
      this.orderService.setTargetOrder(null);
    }
  }
  async handleNewOrder() {
    await this.orderService.updateOrder(this.newOrder.id, {
      preparation_time: this.selectedTime
    });
    this.audioService.stopNotification();
    this.orderService.newOrder(null);
    this.checkNewOrder();
  }
  checkNewOrder() {
    setTimeout(() => {
      this.orderService.checkNewOrderQueue();
    }, 3000);
  }
  async handleOrder() {
    const data: any = {
      type: "order_status",
      store_id: this.state.getValue().profile.profile.store.id,
      user_id: this.state.getValue().profile.id,
      order_id: this.newOrder.id,
      comment: ""
    };
    console.log("our order status ", this.newOrder.status);
    switch (this.newOrder.status) {
      case "confirmed":
        data.status = "accepted";
        break;
      case "accepted":
        data.status = "ready";
        break;
      case "ready":
        data.status = "in-delivery";
        break;
    }
    console.log("state ", this.state.getValue());

    try {
      await this.orderService.updateOrderStatus(data);
      if (this.newOrder.status === "confirmed") {
        this.handleNewOrder();
      }

      this.orderService.setTargetOrder(null);
      this.orderChecked = false;
      this.selectedTime = -1;
      this.targetType = "";
    } catch (err) {
      console.error("error ", err);
    }
  }

  setPreparationTime(time) {
    this.selectedTime = time;
  }

  navigateTo(path) {
    this.router.navigateByUrl(path);
    this.nwServices.expandScreen();
    this.collapsed = false;

    if (path !== "/") {
      this.clicked = 0;
    } else {
      this.openDash();
    }
    console.log("activvvvvvvvvvvvvvvvvv ", this.router.url);
  }
  openDash() {
    this.clicked += 1;
    console.log("cliekced ", this.clicked);
    if (!this.showDiv) {
      this.nwServices.expandScreen();
      this.collapsed = false;
      this.showDiv = !this.showDiv;
    } else {
      if (this.clicked >= 2) {
        this.nwServices.minimizeScreen();
        this.collapsed = true;
        this.showDiv = false;
      }
    }
  }

  openOrderModal() {
    this.modalService.open(this.modal, { centered: true });
  }
  openOrderRejectModal() {
    this.refuseCause = null;
    this.modalService.open(this.rejectModal, { centered: true });
  }
  closeRejectModal(modal) {
    modal.close("Close click");
  }

  print() {
    window.print();
    /* return
    let win = new this.nwServices.remote.BrowserWindow({show: false});
    fs.writeFile(path.join(' C:\\Users\\Moussa\\Desktop\\mydata\\print.txt'), text, {}, () => {

      console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxx",path.join(' C:\\Users\\Moussa\\Desktop\\mydata\\print.txt') )
      win.loadURL(path.join(' C:\\Users\\Moussa\\Desktop\\mydata\\print.txt') )
      win.webContents.on('did-finish-load', () => {
        win.webContents.print({silent: true })
        setTimeout(() => {
          win.close();
        }, 1000);
      })
    })*/
  }
  doprinter = false;
  printRecip() {
    this.printDiv("orderBody");
    return;
    this.printerService.printOpenWindow = false;
    this.doprinter = true;

    this.printerService.printOpenWindow = false;
    this.printerService.printDiv("orderBody");
    this.printerService.printOpenWindow = true;
    /*
    setTimeout(()=>{
      this.printerService.printOpenWindow = false;
      const el =  document.getElementById('orderBody')
      this.printerService.printDiv('print-this' )
    }, 500)

  this.printerService.printOpenWindow = true;
*/
  }
  PrintElem(elem) {
    let mywindow = window.open("", "PRINT", "height=400,width=600");

    mywindow.document.write(
      "<html><head><title>" + document.title + "</title>"
    );
    mywindow.document.write("</head><body >");
    mywindow.document.write("<h1>" + document.title + "</h1>");
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write("</body></html>");

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
  }

  printDiv(divName) {
    let width = window.innerWidth;
    let height = window.innerHeight;
    let myWidth = 250;
    let myHeight = 500;
    this.printing = true;
    let left = 100;
    let top = 200;
    setTimeout(() => {
      const mywindow = window.open(
        "",
        "PRINT",
        "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=" +
          myWidth +
          ", height=" +
          myHeight +
          ", top=" +
          top +
          ", left=" +
          left
      );
      const printContents = document.getElementById(divName).innerHTML;
      mywindow.document.write(
        "<head><style>" +
          " body {padding:0px!important; margin: 0px!important; width: 100%!important;} * { font-size: 14px !important; padding: 0px!important; margin: 0px !important; float: none;} " +
          " #itemPrice {white-space: nowrap !important;} }</style></head>"
      );
      mywindow.document.write(printContents);
      mywindow.document.write("<br><br>");
      mywindow.document.write('<div style="height: 15px"></div>');

      mywindow.document.close();
      console.log("print document ", mywindow.document);
      mywindow.focus();
      mywindow.print();
      this.printing = false;
      setTimeout(() => {
        mywindow.close();
      }, 800);
    }, 500);
  }
}
