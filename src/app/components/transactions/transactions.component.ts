import { Component, OnInit } from '@angular/core';
import { TransactionsService } from '../../services/transactions.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  transactions = []
  meta;
  pages = [1, 2 , 3, 4];
  activePage = 1;
  selectedIndex;
  constructor(private transactionService: TransactionsService) { }

  ngOnInit() {
    this.getTransactions();
  }

  getTransactions(page = 1) {
      this.transactionService.getTransactions(page).subscribe( res => {
        console.log("our transactions ", res);
        this.transactions = res.data;
        this.scrollTabContentToTop();
        this.meta = res.meta;
      })
  }
  nextPage(page) {
    if(this.meta.pagination.total_pages < page ) {
      return;
    }
    if( page > this.pages[3]) {
      this.pages = this.pages.slice(1, this.pages.length);
      this.pages.push(page);
    }
    if( page < this.pages[0]) {
      console.log("current ipages *********  ", this.pages);
      this.pages.unshift(page);
      console.log("current ipages *********  ", this.pages);
      this.pages = this.pages.slice(0, this.pages.length - 1);
      console.log("current ipages *********  ", this.pages);
    }
    this.activePage = page;
      this.getTransactions(page);
  }


  scrollTabContentToTop() : void {
    const elmnt =  document.getElementById("scroll")
    setTimeout(()=>{elmnt.scrollTop = 0;}
    )
  }


}
