import { Component, OnInit, ElementRef } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  FormBuilder
} from "@angular/forms";
import { AuthApiService } from "../../services/auth-api.service";
import { Router } from "@angular/router";
import {NwServices} from "../../providers/nwServices";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  username;
  userform: FormGroup;
  loginError = false;
  formatter = (result: string) => result.toLowerCase();
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authenticateService: AuthApiService,
    private nwServices: NwServices
  ) {}

  ngOnInit () {

   // this.electronService.remote.getCurrentWindow().setContentSize(434, 496, true);

    this.userform = this.fb.group({
      username: new FormControl("", Validators.required),
      password: new FormControl("", [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(32)
      ])
    });
  }
  onSubmit(form, btn: any) {
    this.loginError = false;
    btn.disabled = true;
    this.authenticateService.connect(form).subscribe(
      async (res: any) => {
        const data = res.body;
        console.log("response ", res);
        if (res.status === 200) {
          localStorage.setItem("token", data.token);
          this.router.navigate(["/"]);
        }
      },
      error => {
        this.loginError = true;
        console.log("erorrrrrrrrrrrrrrrrrrr");
        btn.disabled = false;
        if (error.status === 404) {
          console.log("error status ");
        }
      }
    );
  }
}
