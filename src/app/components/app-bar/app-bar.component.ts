import {Component, OnInit, ViewChild} from '@angular/core';
const AppConfig = require("../../../environments/environment");
import { NwServices } from 'src/app/providers/nwServices';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-app-bar',
  templateUrl: './app-bar.component.html',
  styleUrls: ['./app-bar.component.scss']
})
export class AppBarComponent implements OnInit {
  version = AppConfig.version;
  @ViewChild("dialog", { static: true }) modal: any;
  constructor(private nwService: NwServices,  private modalService: NgbModal) { }
  openAlertDialog() {
    this.modalService.open(this.modal, { centered: true });
    this.nwService.expandScreen();
  }
  closeApp() {
     this.nwService.currentWindow.close();
  }
  ngOnInit() {
  }
  minimWindow () {
    const window = this.nwService.currentWindow;
    window.minimize();
  }
  closeWindow() {
    this.openAlertDialog();
  /*  const window = this.electronService.remote.getCurrentWindow();
    let choice = this.electronService.remote.dialog.showMessageBox(
      window,
      {
        type: 'question',
        defaultId: 1,
        buttons: ['Oui', 'Non'],
        title: 'Confirmation',
        message: "Quitter l'application ?"
      });
    if(choice === 1){
    } else {
      window.close();
    }
  */}
}
