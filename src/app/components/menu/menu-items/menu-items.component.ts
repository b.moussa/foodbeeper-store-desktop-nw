import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { MenuService } from "../../../services/menu.service";

import { State } from "@ngrx/store";
import { AppState } from "../../../state/appState";

@Component({
  selector: "app-menu-items",
  templateUrl: "./menu-items.component.html",
  styleUrls: ["./menu-items.component.scss"]
})
export class MenuItemsComponent implements OnInit {
  @Input("menu") menu = [];
  @Input("options") options = [];
  target;
  optionsStatus: boolean;
  @Output("refreshMenu") refershMenu = new EventEmitter();
  constructor(
    public state: State<AppState>,
    private menuService: MenuService
  ) {}
  ngOnInit() {}

  selectedOptionsCategory = [];

  updateItemStatus(item) {
    this.menuService
      .updateItem({ available: !item.available }, item.id)
      .toPromise()
      .then(res => {
        this.refershMenu.emit("refresh");
      });
  }
  updateSectionStatus(option) {
    console.log(option);
   
    if (option.items.every(elem => elem.item.available===true)){
      for (let p of option.items) {
        this.menuService
          .updateItem({ available: false }, p.item.id)
          .toPromise()
          .then(res => {
            this.refershMenu.emit("refresh");
          });
      }
    }else{
      for (let p of option.items) {
        this.menuService
          .updateItem({ available: true }, p.item.id)
          .toPromise()
          .then(res => {
            this.refershMenu.emit("refresh");
          });
      }
    }
  }
}
