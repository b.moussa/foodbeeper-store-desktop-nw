import { Component, OnInit } from "@angular/core";
import { MenuService } from "../../services/menu.service";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.scss"]
})
export class MenuComponent implements OnInit {
  target = "menu";
  menu = [];
  options = [];

  constructor(private menuService: MenuService) {}
  getMenu() {
    this.menuService.getMenu().subscribe(res => {
      this.menu = res.data;
    });
  }
  getOptions() {
    this.menuService.getOptions().subscribe(res => {
      this.options = res.data;
    });
  }
  ngOnInit() {
    this.getMenu();
    this.getOptions();
  }
}
