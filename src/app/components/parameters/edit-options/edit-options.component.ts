import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MenuService } from "../../../services/menu.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { State, Store } from "@ngrx/store";
import { AppState } from "../../../state/appState";
@Component({
  selector: "app-edit-options",
  templateUrl: "./edit-options.component.html",
  styleUrls: ["./edit-options.component.scss"]
})
export class EditOptionsComponent implements OnInit {
  groupId = "";
  optionData: any = { name: "", price: "" };
  optionGroupData: any = { name: "", min: "", max: "" };
  @Input("options") options = [];
  target;
  @Output("refreshMenu") refershMenu = new EventEmitter();
  @ViewChild("content", { static: true }) modal;
  @ViewChild("optionModel", { static: true }) optionModel;
  optionForm: FormGroup;
  updateGroup = false;
  constructor(
    private modalService: NgbModal,
    private menuService: MenuService,
    public state: State<AppState>
  ) {}

  ngOnInit() {
    this.optionForm = new FormGroup({
      name: new FormControl("", {
        validators: [Validators.required, Validators.minLength(4)]
      }),
      price: new FormControl("", { validators: [Validators.required] })
    });
  }
  addOption(group) {
    console.log(group);
    this.openVerticallyCentered();
    this.optionData.group_id = this.groupId;
    this.optionData.name = "";
    this.optionData.price = "";
    // this.optionData = {};
  }
  editOption(item, name) {
    this.optionData = {};
    this.target = { ...item, section: name };
    console.log("ei  ", this.target);
    this.optionData.name = this.target.name;
    this.optionData.price = this.target.price;
    this.openVerticallyCentered();
  }
  openVerticallyCentered() {
    this.modalService.open(this.modal, { centered: true });
  }
  updateOption(btn, isUpdated) {
    const data: any = {};
    if (this.target) {
      btn.disabled = true;
      this.menuService
        .updateOption(this.optionForm.value, this.target.id)
        .toPromise()
        .then(res => {
          this.target = null;
          this.optionForm.reset();
          btn.disabled = false;
          this.modalService.dismissAll();
          this.refershMenu.emit("refresh");
        })
        .catch(err => {
          console.log("eror ", err);
          btn.disabled = false;
          this.target = null;
        });
    } else {
      this.menuService
        .addOption(
          this.state.getValue().profile.profile.store.id,
          this.optionData
        )
        .subscribe(results => {
          this.target = null;
          this.modalService.dismissAll();
          this.refershMenu.emit("refresh");
        });
    }
  }
  openEditModal(group) {
    this.groupId = group.id;
    console.log("this is the edit option function hey");
    console.log(this.groupId);
    this.updateGroup = true;
    this.optionGroupData.name = group.name;
    this.optionGroupData.min = group.min;
    this.optionGroupData.max = group.max;
    this.openDishModel();
  }

  addNewGroup() {
    this.updateGroup = false;
    this.optionGroupData = {};
    this.openDishModel();
  }
  openDishModel() {
    this.modalService.open(this.optionModel, { centered: true });
  }
  addOptionGroup() {
    if (this.updateGroup) {
      this.menuService
        .updateOptionGroup(
          this.state.getValue().profile.profile.store.id,
          this.groupId,
          this.optionGroupData
        )
        .subscribe(result => {
          this.modalService.dismissAll();
          this.updateGroup = false;
          this.refershMenu.emit("refresh");
        });
    } else
      this.menuService
        .addOptionGroup(
          this.state.getValue().profile.profile.store.id,
          this.optionGroupData
        )
        .subscribe(result => {
          this.modalService.dismissAll();
          this.updateGroup = false;
          this.refershMenu.emit("refresh");
        });
  }
}
