import {
  Component,
  EventEmitter,
  Input,
  NgZone,
  OnInit,
  Output,
  ViewChild
} from "@angular/core";
import { MenuService } from "../../../services/menu.service";
import { BsModalService } from "ngx-bootstrap";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { getProfile, ProfileAction } from "../../../state/profile/actions";
import { State, Store } from "@ngrx/store";
import { AppState } from "../../../state/appState";
import { ToastService } from "../../../toasts/toast.service";

@Component({
  selector: "app-edit-menu",
  templateUrl: "./edit-menu.component.html",
  styleUrls: ["./edit-menu.component.scss"]
})
export class EditMenuComponent implements OnInit {
  @Input("menu") menu = [];
  @Input("options") options = [];
  @Input() sectionData = {
    name: ""
  };
  sectionInfo: {} = {};
  sectionId = "";
  target;
  selectedItem = {};
  sectionUpdated = false;
  isDeleteSection = false;
  @Output("refreshMenu") refershMenu = new EventEmitter();
  @ViewChild("content", { static: true }) modal;
  @ViewChild("deleteModal", { static: true }) deleteModal;
  @ViewChild("addSectionModal", { static: true }) addSectionModal;
  @ViewChild("deleteSectionModal", { static: true }) deleteSectionModal;
  @ViewChild("AddSectionsModal", { static: true }) AddSectionsModal;
  updateSection = false;
  dishForm: FormGroup;
  newDishPicUrl;
  imageUpdated = false;
  isUpdate = false;
  imageData;
  photoValidator = false;
  selectedOptions = [];
  updatedTarget: { name?: string; price?: string } = { name: "", price: "" };
  constructor(
    private toastService: ToastService,
    private zone: NgZone,
    public state: State<AppState>,
    private modalService: NgbModal,
    private fb: FormBuilder,
    private menuService: MenuService
  ) {}
  ngOnInit() {
    this.initForm();
    console.log("options are ", this.options);
    console.log();
  }

  onFileSelect(event) {
    const files = event.dataTransfer
      ? event.dataTransfer.files
      : event.target.files;
    this.photoValidator = files.length > 0;
    for (let i = 0; i < files.length; i++) {
      this.imageData = files[i];
    }
    const reader = new FileReader();
    reader.onload = (e: any) => {
      this.imageUpdated = true;
      this.newDishPicUrl = e.target.result;
    };

    reader.readAsDataURL(files[0]);
    this.dishForm.controls["image"].setValue(files[0]);
  }
  selectedOptionsCategory = [];

  deleteItem(btn) {
    btn.disabled = true;
    this.menuService
      .deleteItem(
        this.state.getValue().profile.profile.store.id,
        this.target.id
      )
      .subscribe(
        res => {
          ///    if (res.status == 204) { }
          btn.disabled = false;
          this.modalService.dismissAll();
          this.clearArtifact();

          this.refershMenu.emit("refresh");
        },
        err => {
          console.log("the errror ", err);
          this.modalService.dismissAll();
          this.toastService.show("un erreur est survenue lors la suppression", {
            classname: "bg-danger text-light",
            delay: 10000
          });
          this.clearArtifact();
        }
      );
  }

  selectAllOption(event, option) {
    console.log("waht we have ", this.selectedOptionsCategory);
    const tab = [];
    const target = event.target;
    this.selectedOptionsCategory = [
      ...this.selectedOptionsCategory.filter(i => i === target.value),
      target.value
    ];
    option.options.forEach(item => {
      tab.push(item.option.id);
    });
    if (true) {
      this.selectedOptions = this.selectedOptions.filter(
        el => !tab.includes(el)
      );
      this.selectedOptions = [...this.selectedOptions, ...tab];
      console.log("after adding result is + ", this.selectedOptions);
    } else {
      console.log("filtering ", tab, " with ", this.selectedOptions);
      this.zone.run(() => {
        this.selectedOptions = [
          ...this.selectedOptions.filter(el => !tab.includes(el))
        ];
        console.log("result is -", this.selectedOptions);
      });
    }
  }
  checkIfAllSectionSelected(event, id) {
    if (event.target.checked === false) {
      this.selectedOptions.splice(this.selectedOptions.indexOf(id), 1);
    } else {
      this.selectedOptions.push(id);
    }
  }
  isCheckedOption(id) {
    return this.selectedOptions.indexOf(id);
  }
  initForm() {
    this.dishForm = this.fb.group({
      name: ["", [Validators.required]],
      description: [""],
      price: ["", [Validators.required]],
      image: [""],
      weight: [0],
      // options: [""],
      section_id: [null, [Validators.required]]
    });
  }
  updateForm(target) {
    this.isUpdate = true;
    this.selectedOptions = target.options;
    this.dishForm.patchValue({
      name: target.name,
      description: target.description,
      price: target.price,
      section_id: target.section_id
    });
  }
  editItem(item, section) {
    console.log(item);
    this.target = { ...item, section: section.name, section_id: section.id };
    this.updateForm(this.target);
    this.openVerticallyCentered();
  }
  updateItemStatus(item) {
    console.log(item);

    this.menuService
      .updateItem({ available: !item.available }, item.id)
      .toPromise()
      .then(res => {
        this.refershMenu.emit("refresh");
      });
  }
  clearArtifact() {
    this.imageUpdated = false;
    this.isUpdate = false;
    this.imageData = null;
    this.newDishPicUrl = "";
    this.selectedOptionsCategory = [];
    this.selectedOptions = [];
    this.refershMenu.emit("refresh");
    this.photoValidator = false;
  }
  handleDishForm(btn) {
    btn.disabled = true;
    const formData = new FormData();
    for (const key in this.dishForm.controls) {
      if (key === "image") {
        if (this.photoValidator) {
          formData.append("image", this.imageData, this.imageData.name);
        }
      } else {
        formData.append(key.toString(), this.dishForm.controls[key].value);
      }
    }
    if (this.selectedOptions.length === 0) {
      formData.append("options[]", null);
    } else {
      this.selectedOptions.forEach(element => {
        formData.append("options[]", element);
      });
    }

    if (this.isUpdate) {
      this.menuService
        .updateItem(formData, this.target.id)
        .toPromise()
        .then(res => {
          this.updatedTarget = { name: "", price: "" };
          btn.disabled = false;
          this.clearArtifact();
          this.modalService.dismissAll();
          this.isUpdate = false;
          this.refershMenu.emit("refresh");
        })
        .catch(err => {
          console.log("eror ", err);
          btn.disabled = false;
        });
    } else {
      this.menuService
        .addItem(this.state.getValue().profile.profile.store.id, formData)
        .toPromise()
        .then(res => {
          this.initForm();
          btn.disabled = false;
          this.clearArtifact();
          this.modalService.dismissAll();

          this.refershMenu.emit("refresh");
        })
        .catch(err => {
          console.log("eror ", err);
          btn.disabled = false;
        });
    }
  }
  openDeleteModel(item) {
    this.selectedItem = this.target;
    this.modalService.open(this.deleteModal, { centered: true });
  }
  openVerticallyCentered() {
    this.modalService.open(this.modal, { centered: true });
  }
  addDish(data) {
    data = this.sectionInfo;
    console.log(data);
    this.isUpdate = false;
    this.initForm();
    this.dishForm.controls["section_id"].setValue(data.id);
    this.openVerticallyCentered();
  }
  addsectionModal() {
    this.sectionUpdated = false;
    this.addModal();
    this.sectionData.name = "";
  }
  addModal() {
    this.modalService.open(this.addSectionModal, { centered: true });
  }
  addSection() {
    if (this.sectionUpdated) {
      console.log(this.sectionId);
      this.menuService
        .updateSection(
          this.state.getValue().profile.profile.store.id,
          this.sectionId,
          this.sectionData
        )
        .subscribe(
          result => {
            this.modalService.dismissAll();
            this.sectionUpdated = false;
            this.refershMenu.emit("refresh");
          },
          err => {
            console.log("the errror ", err);
            this.modalService.dismissAll();
            this.toastService.show(
              "un erreur est survenue lors la midification",
              {
                classname: "bg-danger text-light",
                delay: 10000
              }
            );
            this.clearArtifact();
          }
        );
    } else {
      this.menuService
        .addSection(
          this.state.getValue().profile.profile.store.id,
          this.sectionData
        )
        .subscribe(
          data => {
            this.refershMenu.emit("refresh");
            this.sectionUpdated = false;
            this.modalService.dismissAll();
          },
          err => {
            console.log("the errror ", err);
            this.modalService.dismissAll();
            this.toastService.show(
              "un erreur est survenue lors l'ajout d'une nouvelle section",
              {
                classname: "bg-danger text-light",
                delay: 10000
              }
            );
            this.clearArtifact();
          }
        );
      console.log(this.sectionId);
    }
  }
  openEditModal(data) {
    console.log(data);
    this.updateSection = true;
    this.sectionData.name = data.section.name;
    this.addModal();
    this.sectionId = data.section.id;
    this.sectionInfo = data.section;
    this.sectionUpdated = true;
  }
  openDeleteSectionModal() {
    this.modalService.open(this.deleteSectionModal, { centered: true });
  }
  AddSecModal() {
    this.modalService.open(this.AddSectionsModal, { centered: true });
    this.sectionData.name = "";
  }
  deleteSection(btn) {
    this.menuService
      .deleteSection(
        this.state.getValue().profile.profile.store.id,
        this.sectionId
      )
      .subscribe(
        res => {
          this.modalService.dismissAll();
          this.clearArtifact();

          this.refershMenu.emit("refresh");
        },
        err => {
          console.log("the errror ", err);
          this.modalService.dismissAll();
          this.toastService.show("un erreur est survenue lors la suppression", {
            classname: "bg-danger text-light",
            delay: 10000
          });
          this.clearArtifact();
        }
      );
  }
}
