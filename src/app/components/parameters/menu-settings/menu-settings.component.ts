import { Component, OnInit } from "@angular/core";
import { MenuService } from "../../../services/menu.service";
@Component({
  selector: "app-menu-settings",
  templateUrl: "./menu-settings.component.html",
  styleUrls: ["./menu-settings.component.scss"]
})
export class MenuSettingsComponent implements OnInit {
  target = "editMenu";
  menu = [];
  options = [];
  constructor(private menuService: MenuService) {}
  getMenu() {
    this.menuService.getMenu().subscribe(res => {
      this.menu = res.data;
    });
  }
  getOptions() {
    this.menuService.getOptions().subscribe(res => {
      this.options = res.data;
    });
  }
  ngOnInit() {
    this.getMenu();
    this.getOptions();
  }
}
