import { Component, OnInit, ViewChild, OnDestroy, NgZone } from "@angular/core";
import { AppState } from "../../state/appState";
import { getProfile } from "../../state/profile/actions";
import { State, Store } from "@ngrx/store";
const AppConfig = require("../../../environments/environment");
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { StoreService } from "../../services/store.service";
import * as moment from "moment";
import { Observable } from "rxjs/index";
import { TranslateServices } from "../../services/translate.service";
@Component({
  selector: "app-parameters",
  templateUrl: "./parameters.component.html",
  styleUrls: ["./parameters.component.scss"]
})
export class ParametersComponent implements OnInit, OnDestroy {
  showPhones = false;
  version = AppConfig.version;
  @ViewChild("dateModal", { static: false }) dateModal: any;
  @ViewChild("assistanceModal", { static: false }) assistanceModal: any;
  @ViewChild("languageModal", { static: false }) languageModal: any;
  deliveryStatus = 0;
  supportPhones = [];
  formatedDate;
  profile$: Observable<any>;
  dates = ["1hour", "3hours", "day", "Indefinitely"];
  constructor(
    private store: Store<AppState>,
    private zone: NgZone,
    public state: State<AppState>,
    private storeService: StoreService,
    private modalService: NgbModal,
    private translate: TranslateServices
  ) {
    this.profile$ = this.store.select(getProfile);
  }
  option = -1;
  language = "";
  lang = "";
  ngOnInit() {
    this.profile$.subscribe(res => {
      console.log("***************  support phonesss  ", res.user);
      if (res.user && res.user.support_phone) {
        this.supportPhones = res.user.support_phone.split(",");
      }
      if (res.user) {
        this.zone.run(() => {
          this.deliveryStatus = res.user.accept_delivery;
        });
      }
    });
  }
  ngOnDestroy() {}
  openOrderModal() {
    if (this.deliveryStatus == 1) {
      console.log("delivery status is equal to one ");
      this.modalService.open(this.dateModal, { centered: true });
    } else {
      console.log("dellivery status is not equal to one ");
      this.resumeDelivery();
    }
  }
  openAssistanceModal() {
    this.modalService.open(this.assistanceModal, { centered: true });
  }
  settingsLanguageModal() {
    this.lang = localStorage.getItem("lang");
    this.modalService.open(this.languageModal, { centered: true });
    this.language = "";
  }
  async resumeDelivery() {
    const data = { accept_delivery: 1 };
    await this.updateStatus(data);
  }
  setDate(index) {
    this.option = index;
    console.log("our options ", this.option);
  }

  async pauseDelivery(btn1, btn2, modal) {
    let after;
    const dateFormat = "YYYY-MM-DD HH:mm:ss";
    let now = moment().format(dateFormat);
    console.log("now " + now);
    if (this.option === 0) {
      after = moment(now)
        .add(1, "hours")
        .format(dateFormat);
      console.log("afterTime " + after);
      this.formatedDate = after;
    } else if (this.option === 1) {
      after = moment(now)
        .add(3, "hours")
        .format(dateFormat);
      console.log("afterTime " + after);
      this.formatedDate = after;
    } else if (this.option === 2) {
      after = moment(now)
        .add(1, "days")
        .format(dateFormat);
      console.log("afterTime " + after);
      this.formatedDate = after;
    } else if (this.option === 3) {
      this.formatedDate = null;
    }

    btn1.disabled = true;
    btn2.disabled = true;
    try {
      const data = {
        accept_delivery: 0,
        accept_delivery_at: this.formatedDate
      };
      await this.updateStatus(data);
    } catch (err) {}
    btn1.disabled = false;
    btn2.disabled = false;
    modal.close("Close click");
  }

  updateStatus(data) {
    return this.storeService.updateStoreDeliveryStatus(data).toPromise();
  }
  setLang() {
    localStorage.setItem("lang", this.language);
    this.translate.use(this.language);
    this.modalService.dismissAll();
  }
  setLanguage(index) {
    this.language = index;
  }
  closeModal() {
    this.modalService.dismissAll();
  }
}
