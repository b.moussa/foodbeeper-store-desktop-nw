import { Injectable } from '@angular/core';

// If you import a module but never use any of the imported values other than as TypeScript types,
// the resulting javascript file will look as if you never imported the module at all.
/*import { ipcRenderer, webFrame, remote , } from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';
*/
@Injectable()
export class NwServices {

  ipcRenderer: any;
  webFrame: any;
  remote: any;
  childProcess: any;
  fs: any;
  nw = window.nw;
  currentWindow = window.nw.Window.get();
  nwObjectWindow = window.nw.Window;
  constructor() {}

  isElectron = () => {
    return window && window.process && window.process.type;
  }
  expandScreen(height = 700 , width= 434) {
    this.currentWindow.resizeTo(width, height);

  }
  minimizeScreen (height = 130, width = 434) {
    this.currentWindow.resizeTo(width, height);
  }

}
