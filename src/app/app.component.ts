import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
const AppConfig = require("../environments/environment");
declare var fireConfig: any;
import { AuthApiService } from './services/auth-api.service';
import { Store, select } from '@ngrx/store';
import { StoreService } from './services/store.service';
import { authenticateUser } from './state/profile/actions';
import { catchError } from 'rxjs/internal/operators';
import { TranslateServices } from '../app/services/translate.service';
import {NwServices} from './providers/nwServices';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  count$;
  updater
  constructor(
    private authenticateService: AuthApiService,
    private storeService: StoreService,
    public nwServices: NwServices,
    private translate: TranslateService,
    private store: Store<{ profile: any }>,
    private translatee: TranslateService
  ) {
    /*    this.count$ = store.pipe(select('profile'));
    this.authenticateService.authenticatedUser().pipe(
      catchError((err) => {
      console.log('erororrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr ', err);
      return err;
    })).subscribe(res => {
      console.log('oooooooooooooooooooo ', res);
      this.store.dispatch(authenticateUser(res));
      this.storeService.getStoreInformation().toPromise().then(( data )=> {
        console.log('store informationnnnnnnnnnnnnn ', data);
        this.storeService.updateStoreProfile(data.data);
      });
      this.count$.subscribe(r => {
        console.log('holaaaaaaaaaaaaaaaaaaa ', r);
      });

    })
*/

    // translate.setDefaultLang('en');
    // console.log('AppConfig', AppConfig);

    if (nwServices.isElectron()) {
      console.log('Mode electron');
    } else {
      console.log('Mode web');
    }
    const AutoUpdater =  this.nwServices.nw.require('nw-autoupdater');
    this.updater = new AutoUpdater ( this.nwServices.nw.require('package.json') );
 setTimeout(()=>{
   this.runUpdater();
 }, 5200);

  }

  async  runUpdater () {

   //  this.nwServices.nw.Window.get().evalNWBin(null, "./assets/source.bin")  ;
   //  console.log("xzzzzzzzzzzzzzzaaaaaaaaaaaaaarrrr  ", fireConfig)


    console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ", fireConfig)
    return
      try {

        // Download/unpack update if any available
        const rManifest = await this.updater.readRemoteManifest();
        const needsUpdate = await this.updater.checkNewVersion( rManifest );
        if ( !needsUpdate ) {
         console.log("kkkkkkkkkkkkkkkkk `\nApp is up to date...`");
          return;
        }
        confirm( "New release is available. Do you want to upgrade?" )
        if ( !confirm( "New release is available. Do you want to upgrade?" ) ) {
          return;
        }

        // Subscribe for progress events
        this.updater.on( "download", ( downloadSize, totalSize ) => {
           console.log("downloading....********************** ", `Downloading...` );
          console.log( "********************************download progress", Math.floor( downloadSize / totalSize * 100 ), "%" );
        });
        this.updater.on( "install", ( installFiles, totalFiles ) => {

          console.log( "********************************************install progress", Math.floor( installFiles / totalFiles * 100 ), "%" );
        });
        const updateFile = await this.updater.download( rManifest );
        await this.updater.unpack( updateFile );
        alert( `********************************The application will automatically restart to finish installing the update` );
        await this.updater.restartToSwap();
      } catch ( e ) {
        console.error( e );
      }

  }
}
