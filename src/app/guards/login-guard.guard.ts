import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { HttpService } from '../services/http.service';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginGuardGuard implements CanActivate {

  constructor(
    private httpService: HttpService,
    private router: Router) {}


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean  {
    let result = true;
    return this.httpService.get("authenticated_user").pipe(

      map((res) => {
        console.log("response from it ", res);
        return result = true;
      }),
      catchError( err => {
        return of([err]).pipe(map((res) => {

          if (!result) {
            this.router.navigate(["./app/dashboard"]);
          }
          //
          return result;
        })
        );
      })
    );

  }
}
