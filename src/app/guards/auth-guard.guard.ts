import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { HttpService } from '../services/http.service';
import { map, catchError } from 'rxjs/operators';
import {StoreService} from "../services/store.service";
import {AppState} from "../state/appState";
import {authenticateUser} from "../state/profile/actions";
import {Store} from "@ngrx/store";
import {finalize, switchMap} from "rxjs/internal/operators";
import {AuthApiService} from "../services/auth-api.service";
@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {

  constructor(
    private httpService: HttpService, private authService: AuthApiService, private storeService: StoreService, private store: Store<AppState>,
    private router: Router) {
    }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean  {
    localStorage.getItem("token");
    if (!localStorage.getItem("token")) {
      this.router.navigate(["./login"]);
      return false;
    }

    let result = false;
     return this.authService.authenticatedUser().pipe(
      map ( (res) => {
              result = true;
               console.log("profile data ", res);
               this.store.dispatch(authenticateUser(res));
          //     this.storeService.
               return res;
            }),
            switchMap (res => {
              return this.storeService.getStoreInformation().pipe(
                switchMap((res) => {
                   this.storeService.updateStoreProfile(res.data);
                   return  of(result);
                }),
                catchError( err => {
                  console.log("error *********************************** ", err);
                  this.router.navigate(["./login"]);
                  return of(result);
                })
              )
                }
            ),
            catchError( err => {
              console.log("error *********************************** ", err);
              this.router.navigate(["./login"]);
              return of(result);
            })
      );
  }
}
