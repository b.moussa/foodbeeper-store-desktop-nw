import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
declare var fireConfig:any;
import { AppModule } from './app/app.module';
const AppConfig = require("./environments/environment");
var fs = window.nw.require('fs');
var CryptoJS = window.nw.require("crypto-js");

async function  init() {
  let arrayBuffer;
  let url;
  if (AppConfig.production) {
    enableProdMode();
    url = "assets/pak1.bin";
  } else {
    url = "assets/pak2.bin";
  }


  arrayBuffer = await fs.readFileSync(url, 'utf8');
  // const data = arrayBuffer.buffer.slice(arrayBuffer.byteOffset, arrayBuffer.byteOffset + arrayBuffer.byteLength);
 //  window.nw.Window.get().evalNWBin(null, data);
  console.log("fireconfig ", arrayBuffer);

// Decrypt 
  var bytes  = CryptoJS.AES.decrypt(arrayBuffer, 'secret key 123');
  var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
  console.log("fireconfig  result ", decryptedData);


  return;
/*
  var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(arrayBuffer), 'secret key 123').toString();
  console.log("fireconfig  cryoted ", ciphertext);
*/



  platformBrowserDynamic().bootstrapModule(AppModule, {
      preserveWhitespaces: false
    }).catch(err => console.error(err));
}
init();
