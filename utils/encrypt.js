const fs =   require('fs');
var CryptoJS = require("crypto-js");
const args = process.argv.slice(2).reduce(function (acc, arg)  {
  const sp = arg.split('=');
  acc[sp[0]] = sp[1] ;
  return acc },
  {})
var arrayBuffer;
console.log("args ==> ", args);
const key = args.key || null;
const env = args.env || "staging";

if(env == 'staging') {
  arrayBuffer =  fs.readFileSync(__dirname+"/firebase-staging.json", 'utf8');
  const ciphertext = CryptoJS.AES.encrypt(JSON.stringify(arrayBuffer), 'foodbeeperDesktop_S').toString();
  fs.readFile("src/environments/environment.dev.ts", 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    var result = data.replace("fb_holder", ciphertext);

    fs.writeFile("src/environments/environment.dev.ts", result, 'utf8', function (err) {
      if (err) return console.log(err);
    });

  });
} else {
  arrayBuffer =  fs.readFileSync("firebase-prod.json", 'utf8');
  const ciphertext = CryptoJS.AES.encrypt(JSON.stringify(arrayBuffer), 'foodbeeperDesktop_S').toString();
  fs.readFile("src/environments/environment.prod.ts", 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    var result = data.replace("fb_holder", ciphertext);
    fs.writeFile("src/environments/environment.prod.ts", result, 'utf8', function (err) {
      if (err) return console.log(err);
    });
  });
}
/*

 var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(arrayBuffer), 'secret key 123').toString();
  console.log("fireconfig  cryoted ", ciphertext);


 */
